package edu.ucdavis.cs.rdmorning.pseudorandompassphrasegenerator;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.security.SecureRandom;

import edu.ucdavis.cs.rdmorning.Passwords;

public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG = MainActivity.class.getSimpleName();
    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });

            fab.hide();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("firstrun", true)) {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            SecureRandom secureRandom = new SecureRandom();
            long seed = Math.abs(secureRandom.nextLong());

            prefs.edit().putString("seed", "" + seed).apply();
            prefs.edit().putBoolean("firstrun", false).apply();

            Toast.makeText(this,"Random seed has been generated. Go to settings to change.",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void generatePassphraseButton(View view) {
        SharedPreferences sharedPrefs =
                PreferenceManager.getDefaultSharedPreferences(this);

        String wordlist = sharedPrefs.getString(
                getString(R.string.pref_wordlist_key),
                "English 1000");
        String separator = sharedPrefs.getString(
                getString(R.string.pref_separator_key),
                " ");
        String seed = sharedPrefs.getString(
                getString(R.string.pref_seed_key),
                "");
        /*
        //http://stackoverflow.com/questions/3721358/preferenceactivity-save-value-as-integer
        Integer min_words = sharedPrefs.getInt(
            getString(R.string.pref_min_words_key),
                6);
        */

        Integer min_words = Integer.valueOf(sharedPrefs.getString(
                getString(R.string.pref_min_words_key),
                "6"));

        boolean useUppercase = sharedPrefs.getBoolean(
                getString(R.string.pref_uppercase_key),
                false);
        boolean useLowercase = sharedPrefs.getBoolean(
                getString(R.string.pref_lowercase_key),
                false);
        boolean useDigits = sharedPrefs.getBoolean(
                getString(R.string.pref_digit_key),
                false);
        boolean useSpecial = sharedPrefs.getBoolean(
                getString(R.string.pref_special_key),
                false);

        InputStream inputStream;

        try {
            switch (wordlist) {
                case "English 1000":
                    inputStream = getApplicationContext().getResources().openRawResource(R.raw.en1000words);
                    break;
                case "Spanish 1000":
                    inputStream = getApplicationContext().getResources().openRawResource(R.raw.es1000words);
                    break;
                case "OSPD Scrabble":
                    inputStream = getApplicationContext().getResources().openRawResource(R.raw.ospd_scrabble);
                    break;
                case "Printable Ascii":
                    inputStream = getApplicationContext().getResources().openRawResource(R.raw.printable_ascii);
                    break;
                default:
                    inputStream = getApplicationContext().getResources().openRawResource(R.raw.en1000words);
            }

            TextView textView = (TextView) findViewById(R.id.passphraseTextView);
            EditText editText = (EditText) findViewById(R.id.keyEditText);


            String key;
            if (editText != null) {
                key = editText.getText().toString();
            }
            else {
                Log.d(LOG_TAG,"Error: editText == null");
                return;
            }

            Passwords passwords = new Passwords();
            passwords.readFile(inputStream);

            passwords.setSeed(seed)
                    .setSeparator(separator)
                    .setMinLength(min_words)
                    .setUseUpper(useUppercase)
                    .setUseLower(useLowercase)
                    .setUseDigits(useDigits)
                    .setUseSpecials(useSpecial);

            passwords.shuffleWords();

            //Toast.makeText(this,seed.toString(),Toast.LENGTH_SHORT).show();
            if (textView != null) {
                textView.setText(passwords.generatePhrase(key));
            }
            else {
                Log.d(LOG_TAG,"Error: textView == null");
                return;
            }
        }
        catch (Exception e) {
            Toast.makeText(this,"Err!!! Go check the logs.",Toast.LENGTH_SHORT).show();
            Log.e(LOG_TAG,"Error reading file: " + e.toString());
        }
    }

    public void copyButton(View view) {
        TextView textView = (TextView) findViewById(R.id.passphraseTextView);
        String pphrase;

        if (textView != null) {
            pphrase = textView.getText().toString();
        }
        else {
            Log.d(LOG_TAG,"Error: textView == null");
            Toast.makeText(this,"Something went horribly wrong. PassPhrase NOT Copied.",Toast.LENGTH_SHORT).show();
            return;
        }

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("PassPhrase", pphrase);

        clipboard.setPrimaryClip(clip);
        Toast.makeText(this,"PassPhrase Copied.",Toast.LENGTH_SHORT).show();

        // clear clipboard after 30 seconds
        // TODO: add an option for this. Allow user to specify how much time to wait
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setPrimaryClip(ClipData.newPlainText("PassPhrase", ""));
            }
        }, 30000);

    }

    public void clearButton(View view) {
        TextView textView = (TextView) findViewById(R.id.passphraseTextView);
        EditText editText = (EditText) findViewById(R.id.keyEditText);

        if(textView != null) {
            textView.setText("");
        }

        if(editText != null) {
            editText.setText("");
        }

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText("PassPhrase", ""));

        Toast.makeText(this,"All clear.",Toast.LENGTH_SHORT).show();
    }
}
