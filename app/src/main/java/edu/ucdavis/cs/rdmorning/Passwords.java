package edu.ucdavis.cs.rdmorning;

/**
 * Created by morning on 4/22/16.
 */

import java.lang.*;
import java.util.*;
import java.io.*;

/*
1. get input from user
2. pseudo-randomize file with words (use an input if available)
3. choose words from random list. These do not need to be chosen randomly
  but they must be chosen in a repeatable way.
4. display the resulting passphrase to the user.
*/

public class Passwords {

    /* Original list, read from file */
    private ArrayList<String> words;
    /* List shuffled using seed and random */
    private ArrayList<String> shuffledWords;
    /* File to be read */
    private String fileName;
    /* Seed to be used when shuffling words list into shuffledWords */
    private long seed;
    /* Random to be used for pseudo-random functionality */
    private Random random;
    /* separator used to separate different words in the passphrase */
    private String separator = " ";

    /* Passphrase strength options */
    boolean useUpper = false;
    boolean useLower = false;
    boolean useDigits = false;
    boolean useSpecials = false;
    Integer minLength = 6;

    static public final String LOWERCASE_LETTERS = "abcdefghijklmnopqrstuvwxyz";
    static public final String UPPERCASE_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static public final String DIGITS = "0123456789";
    static public final String SPECIAL_CHARACTERS = " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

    /* init:
            words and shuffledWords to empty ArrayList
            fileName to empty string
            seed to nanoTime
            random new Random
     */
    public Passwords()
    {
        words = new ArrayList<>();
        shuffledWords = new ArrayList<>();
        seed = System.nanoTime();
        random = new Random();
    }

    /* init:
            words and shuffledWords to empty ArrayList
            fileName to argument
            seed to nanoTime
            random new Random
        calls:
            readFile()
     */
    public Passwords(String fileName) throws IOException
    {
        this.fileName = fileName;
        words = new ArrayList<>();
        shuffledWords = new ArrayList<>();
        random = new Random();
        readFile();
    }

    /*
        returns:
            words
     */
    public ArrayList<String> getWords() {
        return words;
    }

    /*
        Changes seed to argument
        calls:
            shuffleWords()
     */
    public void shuffleWords(long seed)
    {
        this.seed = seed;
        shuffleWords();
    }

    /*
        Changes seed to stringHashCode( argument )
        calls:
            stringHashCode()
            shuffleWords()
     */
    public void shuffleWords(String key)
    {
        seed = stringHashCode(key);
        shuffleWords();
    }

    /*
        Make sure to initalize seed and words before calling this method
        Will (re)shuffle words using this.seed
        Changes the seed of random to this.seed
    */
    public void shuffleWords()
    {
        shuffledWords = new ArrayList<>( words );
        random.setSeed( this.seed );
        Collections.shuffle(shuffledWords, random);
    }

    /*
        returns:
            shuffledWords
     */
    public ArrayList<String> getShuffledWords() {
        return shuffledWords;
    }

    /*
        (re)initializes the fileName to the argument
     */
    public Passwords setFileName(String filename) {
        fileName = filename;
        return this;
    }

    /*
        returns:
            fileName
     */
    public String getFileName() {
        return fileName;
    }

    /*
        update seed member and seed used by random using argument
    */
    public Passwords setSeed(long seed) {
        this.seed = seed;
        random.setSeed( this.seed );
        return this;
    }

    /*
        calls:
            stringHashCode()
            setSeed(long)

        Uses argument to generate seed that will be set in setSeed method
    */
    public Passwords setSeed(String key) {
        setSeed( stringHashCode( key ) );
        return this;
    }

    /*
        returns:
            seed
     */
    public long getSeed() {
        return seed;
    }

    /*
        returns:
            separator
     */
    public String getSeparator() {
        return separator;
    }

    /*
        Assigns separator to argument
     */
    public Passwords setSeparator(String separator) {
        this.separator = separator;
        return this;
    }

    public boolean getUseUpper() {
        return useUpper;
    }

    public Passwords setUseUpper(boolean val) {
        useUpper = val;
        return this;
    }

    public boolean getUseLower() {
        return useLower;
    }

    public Passwords setUseLower(boolean val) {
        useLower = val;
        return this;
    }

    public boolean getUseDigits() {
        return useDigits;
    }

    public Passwords setUseDigits(boolean val) {
        useDigits = val;
        return this;
    }

    public boolean getUseSpecials() {
        return useSpecials;
    }

    public Passwords setUseSpecials(boolean val) {
        useSpecials = val;
        return this;
    }

    public Integer getMinLength() {
        return minLength;
    }

    /* TODO: Warn user if less than secure */
    public Passwords setMinLength(Integer val) {
        if(val > 0) {
            minLength = val;
        }

        return this;
    }

    /*
        make sure to set filename before calling this method
        Reads each line of the input file and adds each entry to words
    */
    private void readFile () throws IOException
    {
        readFile( new BufferedReader( new FileReader( fileName ) ) );
    }

    public void readFile (InputStream inputStream) throws IOException
    {
        readFile( new BufferedReader(new InputStreamReader( inputStream )) );
    }

    public void readFile (BufferedReader bufferReader) throws IOException {
        String line;

        while ((line = bufferReader.readLine()) != null) {
            words.add(line);
        }

        bufferReader.close();
    }

    /*
        Prints the words
     */
    public void printWords() {
        for( String word : words ) {
            System.out.println(word);
        }
    }

    /*
        Prints the shuffledWords
     */
    public void printShuffledWords() {
        for( String word : shuffledWords ) {
            System.out.println(word);
        }
    }

    /*
        random should have been (re)initialized before calling this method to ensure the same output can be regenerated
        Returns:
            nextInt from random
    */
    private int getNextIndex()
    {
        return random.nextInt();
    }

    /*
        This method should be called after shuffledWords has been initialized
        1. The key argument is used to reseed random
        2. random is used to generate a list of number that will be used to select different words from
            the shuffledWords
        3. The selectedWords are joined using the separator
        4. Send phrase the augmentPhrase to apply complexity rules
        5. The result is returned

        returns:
            string passphrase generated
     */
    public String generatePhrase(String key) {
        long seed = stringHashCode( key );
        StringBuilder passphrase = new StringBuilder();

        int max = shuffledWords.size();
        int index;

        random.setSeed( seed );
        for (int j = 0; j < minLength; j++) {
            if(passphrase.length() > 0) {
                passphrase.append(separator);
            }

            index = getNextIndex();
            passphrase.append( shuffledWords.get( Math.abs( index % max ) ) );
        }

        return augmentPassphrase(passphrase);
    }

    /*
        This method changes the passphrase based on which options the user has selected.
        A percentage of the phrase will be changed, so indexes are randomly selected.
        Depending on which options are enabled each is given an equal number of indexes to change.
        The changes are also done randomly. This picks an index from a array of the specified character types.

        If no options are enabled the original passphrase is returned.

        Returns:
            passphrase modified by enabled options
     */
    private String augmentPassphrase(StringBuilder passphrase) {
        /* at most this percent of the string will be changed unless that percent is less than
            the number of enabled options
         */
        float percentAugmented = 0.15f;
        Integer augments = Math.round( passphrase.length() * percentAugmented );
        Integer activeAugments = 0;

        Long seed = random.nextLong();

        if(useDigits) {
            seed = random.nextLong();
            activeAugments++;
        }

        if(useUpper) {
            seed = random.nextLong();
            activeAugments++;
        }

        if(useLower) {
            seed = random.nextLong();
            activeAugments++;
        }

        if(useSpecials) {
            activeAugments++;
        }

        if (activeAugments > 0) {
            if (augments < activeAugments) {
                augments = activeAugments;
            }

            if(augments > passphrase.length()) {
                return passphrase.toString();
            }

            random.setSeed( seed );
            ArrayList<Integer> nextRandom = new ArrayList<>(augments);
            ArrayList<ArrayList<Integer>> indexes = new ArrayList<>();

            while (augments != nextRandom.size()) {
                int next = Math.abs( random.nextInt() % passphrase.length() );

                if (nextRandom.contains(next)) {
                    continue;
                }

                nextRandom.add(next);
            }

            int chunkSize = nextRandom.size() / activeAugments;

            for (int i = 0; i < activeAugments; i++) {
                indexes.add(new ArrayList<>(nextRandom.subList(i * chunkSize, (i + 1) * chunkSize)));
            }

            if (useSpecials) {
                for (int i : indexes.remove(0)) {
                    passphrase.setCharAt(i,
                            SPECIAL_CHARACTERS.charAt(Math.abs(random.nextInt() % SPECIAL_CHARACTERS.length())));
                }
            }

            if (useUpper) {
                for (int i : indexes.remove(0)) {
                    // http://stackoverflow.com/questions/18304804/what-is-the-difference-between-character-isalphabetic-and-character-isletter-in
                    //if(Character.isAlphabetic( passphrase.charAt(i) )) {
                    if(Character.isLetter( passphrase.charAt(i) )) {
                        passphrase.setCharAt(i,
                                Character.toUpperCase( passphrase.charAt(i)) );
                    }
                    else {
                        passphrase.setCharAt(i,
                                UPPERCASE_LETTERS.charAt(Math.abs(random.nextInt() % UPPERCASE_LETTERS.length())));
                    }
                }
            }

            if (useLower) {
                for (int i : indexes.remove(0)) {
                    if(Character.isLetter( passphrase.charAt(i) )) {
                        passphrase.setCharAt(i,
                                Character.toLowerCase( passphrase.charAt(i)) );
                    }
                    else {
                        passphrase.setCharAt(i,
                                LOWERCASE_LETTERS.charAt(Math.abs(random.nextInt() % LOWERCASE_LETTERS.length())));
                    }
                }
            }

            if (useDigits) {
                for (int i : indexes.remove(0)) {
                    passphrase.setCharAt(i,
                            DIGITS.charAt(Math.abs(random.nextInt() % DIGITS.length())));
                }
            }
        }

        return passphrase.toString();
    }

    /*
     * Returns a hash code for this string.
     * (The hash value of the empty string is zero.)
     *
     * @return  a hash code value for this object.
     */
    static public long stringHashCode(String string)
    {
        long hash = 0;
        for(int i = 0; i < string.length(); i++)
        {
            // 101111 is a magic prime number
            hash = 101111*hash + string.charAt(i);

            // the java implementation of hashing a string
            // used to return 32 bit hash
            //hash = 31*hash + string.charAt(i);
        }
        return hash;
    }
}