package edu.ucdavis.cs.rdmorning.pseudorandompassphrasegenerator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.security.SecureRandom;

public class SetSeedActivity extends AppCompatActivity {

    private final String LOG_TAG = SetSeedActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_seed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        catch (NullPointerException e) {
            Log.e(LOG_TAG, "onCreate: " + e);
        }
    }


    public void enterSeedButton(View v)
    {
        EnterSeedDialogFragment enterSeedDialogFragment = new EnterSeedDialogFragment();
        enterSeedDialogFragment.show(getFragmentManager(),LOG_TAG);
    }

    public void generateSeedButton(View v)
    {
        GenerateSeedDialogFragment generateSeedDialogFragment = new GenerateSeedDialogFragment();
        generateSeedDialogFragment.show(getFragmentManager(),LOG_TAG);
    }

    public static class GenerateSeedDialogFragment extends DialogFragment {

        private final String LOG_TAG = GenerateSeedDialogFragment.class.getSimpleName();

        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(this.getString( R.string.set_seed_warning ))
                    .setPositiveButton("Generate Seed", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SecureRandom secureRandom = new SecureRandom();
                            long seed = Math.abs(secureRandom.nextLong());

                            SharedPreferences sharedPrefs =
                                    PreferenceManager.getDefaultSharedPreferences(getActivity());
                            sharedPrefs.edit().putString("seed", "" + seed).apply();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

    public static class EnterSeedDialogFragment extends DialogFragment {

        private final String LOG_TAG = EnterSeedDialogFragment.class.getSimpleName();

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            builder.setView(inflater.inflate(R.layout.enter_seed_dialog, null))
                    // Add action buttons
                    .setPositiveButton("Set Seed", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            final AlertDialog alertDialog = (AlertDialog) dialog;
                            EditText editText = (EditText) alertDialog.findViewById(R.id.seedEditText);
                            String seed = "";

                            if(editText != null) {
                                seed = editText.getText().toString();
                            }
                            else {
                                Log.e(LOG_TAG, "editText is null");
                            }

                            SharedPreferences sharedPrefs =
                                    PreferenceManager.getDefaultSharedPreferences(getActivity());
                            sharedPrefs.edit().putString("seed", seed).apply();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            return builder.create();
        }
    }

}
