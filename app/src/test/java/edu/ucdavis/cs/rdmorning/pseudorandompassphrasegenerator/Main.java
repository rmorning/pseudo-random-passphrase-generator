package edu.ucdavis.cs.rdmorning;

import java.lang.*;
import java.util.*;

import static edu.ucdavis.cs.rdmorning.Passwords.stringHashCode;
//import info.debatty.java.stringsimilarity.*;

public class Main {

    static final String fileName
            //= "res/wordlists/1000-words-es.txt";
            //= "res/wordlists/letters.txt";
            = "res/wordlists/1-1000-github-deekayen-4148741.txt";
            //= "res/wordlists/dictionary-github-jonbcards-scrabble-bot.txt"; // large
            //= "res/wordlists/ospd-scrabble.txt"; // large

    /* Test for hashing function stringHashCode(string) */
    static public void testHashing()
    {
        String[] strings = {""," ","  ","   ","    ","     ","      ",
                "a","ab","abc","abcd","abcde","abcdef",
                "1","2","3","4","5","6"
        };

        for(String s : strings) {
            System.out.printf("%-10s: %d\n", s, stringHashCode(s));
            //System.out.printf("%-10s: %d  %d\n", s, stringHashCode(s), s.hashCode());
        }
    }

    /* Generates 1000 passphrase using 0000..1000 as key */
    static public void test1000()
    {
        Passwords passwords = new Passwords(fileName);
        passwords.setSeed( stringHashCode("") );
        //passwords.setSeed( stringHashCode("Nice night for a walk?" ) );
        passwords.shuffleWords();

        //for(Integer i = 0; i < 1001; i++) {
        for(Integer i = 0; i < 10; i++) {
            String s = String.format("%04d", i);
            //System.out.printf("%s: ", s);

            try {
                System.out.println(passwords.generatePhrase(s));
            } catch(Exception e) {
                System.out.println("EXCEPTION: " + e.toString());
            }
        }
    }

    /* test to check if different initialization seeds create unique lists */
    static public void testShuffle(int times)
    {
        Passwords passwords = new Passwords(fileName);
        passwords.setSeed( stringHashCode("" ) );
        passwords.shuffleWords();

        long seed;
        ArrayList<String> shuff;

        for(int i = 0; i < times; i++) {
            passwords.shuffleWords( Long.MAX_VALUE - i );
            seed = passwords.getSeed();
            shuff = new ArrayList<String>(passwords.getShuffledWords());//Passwords.getWords()

            StringBuilder storage = new StringBuilder();

            for (int j = 0; j < shuff.size(); j++) {
                storage.append(shuff.get(j) + " ");
            }

            //System.out.print(String.format("%-20s, %-20s, ", stringHashCode( storage.toString() ), seed) + storage.toString() + "\n");
            System.out.println( storage.toString() );
        }
    }

    /* test to ensure Passwords.GetNextIndex() is uniformly distributed */
    /* TODO test with many different seeds */
    static public void testGetNextIndex() {
        String init = "";
        Passwords passwords = new Passwords();
        System.out.println("hash from function: " + stringHashCode(init) );
        passwords.setSeed( stringHashCode(init) );
        System.out.println("passwords.seed: " + passwords.getSeed());

        StringBuilder key = new StringBuilder();

        String lower = "abcdefghijklmnopqrstuvwxyz";
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String num = "0123456789";

        for(int i = 0; i < 100000; i++) {
            if(i%52 < 26)
            {
                key.append( lower.charAt(i % 26) );
            }
            else
            {
                key.append( upper.charAt(i % 26) );
            }
        }

        //System.out.println("key: " + key);

        int max = 10;
        int[] dist = new int[max];
        //int max = key.length();
        int index = 0;

        Random random = new Random( stringHashCode(key.toString()) );
        for (int j = 0; j < key.length(); j++) {
            index = passwords.getNextIndex();
            //System.out.println(Math.abs( index % max ));
            dist[ Math.abs(index % max) ] += 1;
        }

        int dmin = dist[0], dmax = dist[0];
        for (int j = 0; j < max; j++) {
            if(dist[j] < dmin) dmin = dist[j];
            if(dist[j] > dmax) dmax = dist[j];

            System.out.println(j + ": " + dist[ j ]);
        }

        Statistics stats = new Statistics(dist);
        System.out.println("mean: " + stats.getMean());
        System.out.println("stddev: " + stats.getStdDev());
        System.out.println("var: " + stats.getVariance());
        System.out.println("min: " + dmin + " max: " + dmax);
    }

    static  public void testSimilarity()
    {
        // using same seed and different keys

        // using different seed and same keys
    }

    /* testing different separators */
    static public void testSeparator() {
        Passwords passwords = new Passwords(fileName);
        passwords.setSeed( stringHashCode("") );
        passwords.shuffleWords();
        passwords.setSeparator("*");

        int numPhrases = 5;

        for(Integer i = 0; i < numPhrases; i++) {
            String s = String.format("%04d", i);
            //System.out.printf("%s: ", s);

            try {
                System.out.println(passwords.generatePhrase(s));
            } catch(Exception e) {
                System.out.println("EXCEPTION: " + e.toString());
            }
        }
    }

    /*
        Test for enabling options: useUpper, useDigits, useSpecials, minLength
     */
    static public void  testOptions()
    {
        Passwords passwords = new Passwords(fileName);
        passwords.setSeed( stringHashCode("Nice night for a walk?") );
        passwords.shuffleWords();

        int numPhrases = 30;

        System.out.println("PASSPHRASES WITH NO OPTIONS SET");
        for(Integer i = 0; i < numPhrases; i++) {
            String s = String.format("%04d", i);

            try {
                System.out.println(passwords.generatePhrase(s));
            } catch(Exception e) {
                System.out.println("EXCEPTION: " + e.toString());
            }
        }

        passwords.setMinLength(8);
        passwords.setUseDigits(true);
        passwords.setUseUpper(true);
        /* passwords.setUseLower(true); */
        passwords.setUseSpecials(true);
        passwords.setSeparator(" ");

        System.out.println("\nPASSPHRASES WITH NUMS, UPPER, SPECIAL, LENGTH");
        for(Integer i = 0; i < numPhrases; i++) {
            String s = String.format("%04d", i);

            try {
                System.out.println(passwords.generatePhrase(s));
            } catch(Exception e) {
                System.out.println("EXCEPTION: " + e.toString());
            }
        }
    }

    static public void main(String[] argv)
    {
        System.out.println("testGetNextIndex()");
        System.out.println("=========================");
        testGetNextIndex();

        System.out.println("\ntest1000()");
        System.out.println("=========================");
        test1000();

        System.out.println("\ntestHashing()");
        System.out.println("=========================");
        testHashing();

        /*
        System.out.println("\ntestShuffle()");
        System.out.println("=========================");
        testShuffle(5);
        */

        System.out.println("\ntestSeparator()");
        System.out.println("=========================");
        testSeparator();

        System.out.println("\ntestOptions()");
        System.out.println("=========================");
        testOptions();
    } // END MAIN
}
